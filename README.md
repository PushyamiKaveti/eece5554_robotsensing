# EECE5554_RobotSensing

### This is a sample repository structure for homeworks ###
### Please follow these conventions ###

You are required to only push your ROS PACKAGES under the respective homework folders not the entre workspace.

This is how a ROS catkin workspace looks like
```
catkin_ws
    |
    |--> devel
    |--> build
    |--> src
           |
           |--> ros_pkg1
                   |
                   |--> CMakeLists.txt
                   |--> package.xml
                   |--> scripts
                          |--> pythonfile_1.py
                          ...
                          ... 
                          |--> pythonfile_n.py
                   |--> msg
                         |--> type_1.msg
                         ...
                         ...
                         |--> type_n.msg

                   |--> srv 
                   |--> src
                   |--> include
           |--> ros_pkg2
           |--> ros_pkg3
           ...
           ...
           |--> ros_pkgn
```
Since this class requires students to write only python drivers and custome messages for them. A ros_pkg typically has only following components.\
**CMakeLists.txt**\
**package.xml**\
**scripts**\
**msg**
### NOTE ###
**1. For home work submission you need to push only your ros packages under LAB folders**\
**2. Please use the same names for the python drivers as shown in the sample LAB folders**\
**3. Please do not hard code your port name (eg. /dev/ttyUSB0') inside your code. You can pass it as a sys argument or rosparam as shown in the sample depth_node_resource_lab_1.py file provided.**\
**4. If you do not follow these conventions in your homework eg. different names for drivers, more than one script to run your driver etc., write up a clear README indicating how to run your code**
